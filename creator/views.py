from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
import django_filters.rest_framework
from creator.models import Album, Song, Playlist, Comment, FavoriteSong, FavoriteAlbum, FavoriteArtist
from creator.permissions import BelongsToArtistPermission, SongBelongsToUserPermission
from creator.serializers import AlbumSerializer, SongSerializer, PlaylistSerializer, CommentSerializer
from account_management.serializers import UserSerializer
from account_management.models import User
from rest_framework.response import Response
from rest_framework import status, exceptions
from django.db import IntegrityError


class AbstractAuthenticatedView(ModelViewSet):
    def get_permissions(self):
        if self.action in ["update", "destroy", "update_partial"]:
            self.permission_classes = [IsAuthenticated, BelongsToArtistPermission]
        if self.action in ["create"]:
            self.permission_classes = [IsAuthenticated]
        if self.action in ["list", "retrieve"]:
            self.permission_classes = []
        return super(AbstractAuthenticatedView, self).get_permissions()


class AlbumViewSet(AbstractAuthenticatedView):
    serializer_class = AlbumSerializer
    queryset = Album.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['user_id']

    def get_queryset(self):
        if self.request.user.is_authenticated:
            userpk = self.request.user
        else:
            userpk = -1
        return Album.objects.filter(Q(artist=userpk) | Q(is_private=False))


class SongViewSet(ModelViewSet):
    serializer_class = SongSerializer

    def get_permissions(self):
        if self.action in ["update", "destroy", "update_partial", "create"]:
            self.permission_classes = [IsAuthenticated, SongBelongsToUserPermission]
        if self.action in ["list", "retrieve"]:
            self.permission_classes = []
        return super(SongViewSet, self).get_permissions()

    def get_queryset(self):
        if self.request.user.is_authenticated:
            userpk = self.request.user
        else:
            userpk = -1
        return Song.objects.filter(Q(album__artist=userpk) | Q(album__is_private=False))


class PlaylistViewSet(AbstractAuthenticatedView):
    serializer_class = PlaylistSerializer

    def get_queryset(self):
        return Playlist.objects.filter(Q(creator=self.request.user))


class CommentViewSet(AbstractAuthenticatedView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.filter(Q(user=self.request.user))


class FavoriteSongViewSet(AbstractAuthenticatedView):
    serializer_class = SongSerializer

    def get_permissions(self):
        if self.action in ["create", "list"]:
            self.permission_classes = [IsAuthenticated]
        return super(FavoriteSongViewSet, self).get_permissions()

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Song.objects.filter(in_favorites__user_id=self.request.user)
        else:
            raise exceptions.AuthenticationFailed('No such user')

    def create(self, request, *args, **kwargs):
        try:
            song_id = request.data.get('song_id')
            user_id = self.request.user.id
            song_creator = User.objects.get(albums__songs=song_id).id
            # id user tries to like their own song return 400
            if user_id == song_creator:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            try:
                # if the user has liked the song - remove it from favorite
                FavoriteSong.objects.get(Q(song_id=song_id), Q(user_id=user_id)).delete()
            except FavoriteSong.DoesNotExist:
                # else add it to favorite
                FavoriteSong.objects.create(song_id=song_id, user_id=user_id)
            return Response(status=status.HTTP_201_CREATED)
        except (IntegrityError, User.DoesNotExist):
            return Response(status=status.HTTP_400_BAD_REQUEST)


class FavoriteAlbumViewSet(AbstractAuthenticatedView):
    serializer_class = AlbumSerializer

    def get_permissions(self):
        if self.action in ["create", "list"]:
            self.permission_classes = [IsAuthenticated]
        return super(FavoriteAlbumViewSet, self).get_permissions()

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Album.objects.filter(in_favorites__user_id=self.request.user.id)
        else:
            raise exceptions.AuthenticationFailed('No such user')

    def create(self, request, *args, **kwargs):
        try:
            album_id = request.data.get('album_id')
            user_id = self.request.user.id
            album_creator = User.objects.get(albums=album_id).id
            # if user tries to like their own album return 400
            if user_id == album_creator:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            try:
                FavoriteAlbum.objects.get(Q(album_id=album_id), Q(user_id=user_id)).delete()
            except FavoriteAlbum.DoesNotExist:
                FavoriteAlbum.objects.create(album_id=album_id, user_id=user_id)
            return Response(status=status.HTTP_201_CREATED)
        except (IntegrityError, User.DoesNotExist):
            return Response(status=status.HTTP_400_BAD_REQUEST)


class FavoriteArtistViewSet(AbstractAuthenticatedView):
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action in ["create", "list"]:
            self.permission_classes = [IsAuthenticated]
        return super(FavoriteArtistViewSet, self).get_permissions()

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return User.objects.filter(in_favorites__user_id=self.request.user.id)
        else:
            raise exceptions.AuthenticationFailed('No such user')

    def create(self, request, *args, **kwargs):
        try:
            artist_id = int(request.data.get('artist_id'))
            if len(User.objects.filter(id=artist_id)) == 0:
                return Response(data={"user": "User does not exist"}, status=status.HTTP_400_BAD_REQUEST)
            if artist_id == self.request.user.id:
                # user cannot add himself/herself to favorite
                return Response(data={"user": "You cannot like yourself."}, status=status.HTTP_400_BAD_REQUEST)
            else:
                try:
                    FavoriteArtist.objects.get(Q(artist_id=artist_id), Q(user_id=self.request.user.id)).delete()
                except FavoriteArtist.DoesNotExist:
                    FavoriteArtist.objects.create(artist_id=artist_id, user_id=self.request.user.id)
                return Response(status=status.HTTP_201_CREATED)
        except IntegrityError:
            return Response(status=status.HTTP_400_BAD_REQUEST)