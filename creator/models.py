from django.core.exceptions import ValidationError
from django.db import models


def cover_path(instance, filename):
    return f'user_{instance.album.artist.id}/{instance.album.id}/cover/{filename}'


def validate_image_extension(value):
    import os
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.png', '.jpeg', '.gif', '.jpg']
    if ext not in valid_extensions:
        raise ValidationError(u'File not supported!')


def song_path(instance, filename):
    return f'user_{instance.album.artist.id}/{instance.album.id}/{filename}'


def validate_file_extension(value):
    import os
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.mp3', '.wav', '.mp4']
    if ext not in valid_extensions:
        raise ValidationError(u'File not supported!')


class Album(models.Model):
    artist = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="albums")
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=1000, blank=True, default='')
    views = models.IntegerField(default=0)
    cover_image = models.ImageField(upload_to=cover_path, validators=[validate_image_extension], blank=True)
    is_private = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Song(models.Model):
    name = models.CharField(max_length=255)
    views = models.IntegerField(default=0)
    description = models.CharField(max_length=1000, blank=True, default='')
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name="songs")
    data = models.FileField(upload_to=song_path, validators=[validate_file_extension])
    rating = models.FloatField(max_length=5, default=0)

    def __str__(self):
        return self.name


class Playlist(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=2000)
    artist = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="playlists")

    def __str__(self):
        return self.name


class SongsInPlaylist(models.Model):
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE, related_name="songs_in_playlist")#different names
    song = models.ForeignKey(Playlist, on_delete=models.CASCADE, related_name="song_in_playlists")#different names                                                                                            #have favorited that contents


class Comment(models.Model):
    user = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="comments")
    date_of_creation = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now_add=True)
    message = models.CharField(max_length=1000)


class CommentAlbum(models.Model):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, related_name="commentsAlbum")
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name="commentsAlbum")


class CommentSong(models.Model):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, related_name="commentsSong")
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name="commentsSong")


class Stream(models.Model):
    artist = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="streams")
    data = models.FileField()


class FavoriteSong(models.Model):
    user = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="fav_songs")
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name="in_favorites")


class FavoriteAlbum(models.Model):
    user = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="fav_albums")
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name="in_favorites")


class FavoriteArtist(models.Model):
    user = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="fav_artists")
    artist = models.ForeignKey("account_management.User", on_delete=models.CASCADE, related_name="in_favorites")