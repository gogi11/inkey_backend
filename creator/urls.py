from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'albums', views.AlbumViewSet, basename='album')
router.register(r'songs', views.SongViewSet, basename='song')
router.register(r'playlists', views.PlaylistViewSet, basename='playlist')
router.register(r'comments', views.CommentViewSet, basename='comment')
router.register(r'favorite/songs', views.FavoriteSongViewSet, basename='favorite_song')
router.register(r'favorite/albums', views.FavoriteAlbumViewSet, basename='favorite_album')
router.register(r'favorite/artists', views.FavoriteArtistViewSet, basename='favorite_artist')

urlpatterns = [
    path("", include(router.urls)),
]
