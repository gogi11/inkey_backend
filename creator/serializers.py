from rest_framework.exceptions import ValidationError

from creator.models import Album, Song, Playlist, Comment
from rest_framework import serializers


class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = "__all__"
        extra_kwargs = {
            'views': {'read_only': True},
            'artist': {'read_only': True},
        }

    def create(self, validated_data):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            album = Album.objects.create(name=validated_data["name"], artist=user)
            return album
        raise ValidationError("You need to be logged in to create an album!")


class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Song
        fields = "__all__"
        extra_kwargs = {
            'views': {'read_only': True},
        }


class PlaylistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Playlist
        fields = "__all__"


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"
        extra_kwargs = {
            'date_of_creation': {'read_only': True},
            'last_edited': {'read_only': True},
        }

