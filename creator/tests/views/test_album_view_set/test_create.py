from http import HTTPStatus
from django.urls import reverse
from account_management.tests.mixin.test_user_mixin import TestUserMixin


class TestCreateAlbumView(TestUserMixin):
    def test_create_album(self):
        response = self.client.post(reverse("album-list"), {"name": "new album"})
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        response_json = response.json()
        self.assertEqual(response_json["name"], "new album")
        self.assertEqual(response_json["artist"], self.user.pk)
        self.assertNotEqual(response_json["artist"], self.user_other.pk)

    def test_create_album_without_name_raises_error(self):
        response = self.client.post(reverse("song-list"), {"artist": self.user.pk})
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

    def test_create_album_without_artist_raises_error(self):
        response = self.client.post(reverse("song-list"), {"name": "haha"})
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

    def test_create_album_unauthenticated(self):
        response = self.client_unauthenticated.post(reverse("album-list"), {"name": "new album"})
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    def test_create_album_with_other_user_id_still_creates_it_to_you(self):
        response = self.client.post(reverse("album-list"), {"name": "new album", "artist": self.user_other.pk})
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        response_json = response.json()
        self.assertEqual(response_json["name"], "new album")
        self.assertEqual(response_json["artist"], self.user.pk)
