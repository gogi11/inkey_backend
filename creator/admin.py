from django.contrib import admin
from .models import Album
from .models import Song
from .models import Playlist
from .models import SongsInPlaylist
from .models import Comment
from .models import Stream
from .models import CommentAlbum, CommentSong
from .models import FavoriteAlbum
from .models import FavoriteSong
from .models import FavoriteArtist


# Register your models here.
admin.site.register(Album)
admin.site.register(Song)
admin.site.register(Playlist)
admin.site.register(SongsInPlaylist)
admin.site.register(Comment)
admin.site.register(CommentAlbum)
admin.site.register(CommentSong)
admin.site.register(FavoriteAlbum)
admin.site.register(FavoriteSong)
admin.site.register(FavoriteArtist)
admin.site.register(Stream)
